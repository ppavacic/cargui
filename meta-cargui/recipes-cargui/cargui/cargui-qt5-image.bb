DESCRIPTION = "GUI for cars made in Qt5"
LICENSE = "LGPLv3"

# require recipes-core/images/core-image-minimal.bb

inherit core-image
inherit populate_sdk_qt5

PACKAGECONFIG_append = " gles2 eglfs "

IMAGE_INSTALL_append = " \
 packagegroup-core-ssh-openssh openssh-sftp-server \
 qtbase qtbase-plugins qtserialport \
 qtmultimedia qtimageformats qtgraphicaleffects \
 qtdeclarative qtquickcontrols \
 vim grep sed procps rsync \
 qtquickcontrols2 qtquickcontrols2-qmlplugins \
 qtquickcontrols qtquickcontrols-qmlplugins \
 ttf-dejavu-sans ttf-ubuntu-mono ttf-ubuntu-sans \
 qtmultimedia qtmultimedia-plugins qtmultimedia-qmlplugins \
 gstreamer1.0  gstreamer1.0-plugins-good \
"

